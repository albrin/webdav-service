FROM oliverlorenz/docker-nginx-webdav

RUN apt-get update && \
    apt-get install -y \
    apache2-utils

#RUN htpasswd -b -c /etc/nginx/htpasswd ${WEBDAV_USER} ${pass}
RUN htpasswd -b -c /etc/nginx/htpasswd nginx pass
