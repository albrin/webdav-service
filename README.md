# WebDAV service

## Introduction

Runs a WebDAV service based in NGINX for local development. The data is protected by basic authentication.

Every user has its own directory. To set the users link in `/etc/nginx/htpasswd`.

## Usage

```bash
docker-compose up
```

For macOS:

```text
general.upload.webdav.server = 'docker.for.mac.localhost'
general.upload.webdav.port = 8888
```
